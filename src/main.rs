use std::env;
use std::sync::Arc;

use anyhow::Result;
use serenity::{
    async_trait,
    client::bridge::gateway::ShardManager,
    model::{gateway::Ready, id::GuildId},
    prelude::*,
};
use structopt::StructOpt;

use log::*;

#[macro_use]
extern crate lazy_static;

#[derive(Debug, StructOpt)]
#[structopt(name = "discord-scan-channel", about = "Lists channels on a server.")]
struct Opt {
    server_id: u64,
    #[structopt(long = "verbose", short = "v")]
    verbose: bool,
}

lazy_static! {
    static ref CONFIG: Opt = Opt::from_args();
}

struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);

        let mut channels = GuildId(CONFIG.server_id)
            .channels(&ctx.http)
            .await
            .expect("Err")
            .into_iter()
            .map(|(_, v)| v)
            .collect::<Vec<_>>();
        channels.sort_by_key(|a| a.position);

        for channel in channels {
            if CONFIG.verbose {
                println!("{:#?}", channel);
            } else {
                println!(
                    "kind: {:?}, name: {}, id: {}",
                    channel.kind, channel.name, channel.id
                );
            }
        }
        if let Some(manager) = ctx.data.write().await.get::<ShardManagerContainer>() {
            manager.lock().await.shutdown_all().await;
        }
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();

    // Configure the client with your Discord bot token in the environment.
    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

    let mut client = Client::new(&token).event_handler(Handler).await?;

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(Arc::clone(&client.shard_manager));
    }

    client.start().await?;
    Ok(())
}
