# discord-scan-channels

A tool to print information about discord channels that are available to the given user or bot. The API gives access to metadata on all channels on a server, not just the ones you can see in your discord client.

## Usage

Obtain a discord token for your account by following `$current_year` method (local storage/inspecting network request's authorization header to /api/v6 or higher).
Obtain discord server id enabling developer mode and copying the id from the server.

```
export DISCORD_TOKEN='a discord token of the desired account'
cargo make run -- $discord_server_id -v
```

## Dependencies

```
cargo install cargo-make cargo-patch
```

## Example

Verbose (`-v`), anonymized output from a channel, displaying what kind data is available.

```
GuildChannel {
    id: ChannelId(
        11111111111111111,
    ),
    bitrate: None,
    category_id: Some(
        ChannelId(
            11111111111111111,
        ),
    ),
    guild_id: GuildId(
        11111111111111111,
    ),
    kind: Text,
    last_message_id: Some(
        MessageId(
            11111111111111111,
        ),
    ),
    last_pin_timestamp: None,
    name: "a-secret-test-channel",
    permission_overwrites: [
        PermissionOverwrite {
            allow: (empty),
            deny: CREATE_INVITE | MANAGE_CHANNELS | READ_MESSAGES | SEND_MESSAGES | MANAGE_MESSAGES | MENTION_EVERYONE | MANAGE_ROLES | MANAGE_WEBHOOKS,
            kind: Role(
                RoleId(
                    11111111111111111,
                ),
            ),
        },
        PermissionOverwrite {
            allow: READ_MESSAGES,
            deny: SEND_MESSAGES,
            kind: Role(
                RoleId(
                    11111111111111111,
                ),
            ),
        },
        PermissionOverwrite {
            allow: READ_MESSAGES,
            deny: SEND_MESSAGES,
            kind: Role(
                RoleId(
                    11111111111111111,
                ),
            ),
        },
    ],
    position: 4,
    topic: Some(
        "A topic which has been set.",
    ),
    user_limit: None,
    nsfw: false,
    slow_mode_rate: Some(
        0,
    ),
    _nonexhaustive: (),
}
```
